import java.util.Scanner;

public class GcdTest{

	static int gcdFinder(int a,int b){
		if(b != 0) return gcdFinder(b,a%b);
		else 
			return a;
	}

	public static void main(String[] args){
		int a;
		int b;
		Scanner cin = new Scanner(System.in);
		a = cin.nextInt();
		b = cin.nextInt();
		System.out.println(gcdFinder(a,b));
	}	
}