import java.util.Scanner;

public class FibonacciTest{

	static int fibonacciElement(int a){
		int fibSeries;
		if(a == 1 || a == 0) return a;
		fibSeries = fibonacciElement(a-1) + fibonacciElement(a-2);
		return fibSeries;
	}

	public static void main(String[] args){
		Scanner cInt = new Scanner(System.in);
		int userInput = cInt.nextInt();
		System.out.println(fibonacciElement(userInput));
	}
}