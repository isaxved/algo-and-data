import java.util.Scanner;

public class FactorialTest{

	public int factorialFinder(int a){
		if (a==0 || a == 1) return a;
		a = a * factorialFinder(a-1);
		return a; 
	}

	public static void main(String[] args){
		Scanner cInt = new Scanner(System.in);
		int someNumber;
		someNumber = cInt.nextInt();
		//refer to function & create an object
		FactorialTest factObj = new FactorialTest();
		System.out.println(factObj.factorialFinder(someNumber));
	}
}