import java.util.Scanner;

public class StringTest{

	static String stringCheck(String some){
		boolean allDigits = true;
		for(char c : some.toCharArray()){
			allDigits = allDigits && Character.isDigit(c);
		}
		if(allDigits == true) return "Yes";
		else 
			return "No";
	}
	

	public static void main(String[] args){
		String someUser;
		Scanner cin = new Scanner(System.in);
		someUser = cin.nextLine();
		System.out.println(stringCheck(someUser));

	}
}