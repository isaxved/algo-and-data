import java.util.Scanner;
import java.util.Arrays;

public class FunctionTest{

	static int returnSmallest(int age){
		int[] userArray = new int[age];
		//Created scan for elements 
		Scanner userScan = new Scanner(System.in);
		System.out.println("Elements: ");
		//Enter the elements
		for(int i = 0; i < age;i++){
			userArray[i] = userScan.nextInt();
		}
		//Sorting the array for min
		Arrays.sort(userArray);
		
		//return userArray[0];
		return userArray[0];
	}

	public static void main(String[] args){
				int userInput;
				Scanner userScan = new Scanner(System.in);
				System.out.println("Size: ");
				userInput = userScan.nextInt();
				System.out.println(returnSmallest(userInput));
	}
}